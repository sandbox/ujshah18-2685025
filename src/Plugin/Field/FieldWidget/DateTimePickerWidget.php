<?php
/**
 * @file
 * Contains \Drupal\date_time_picker\Plugin\Field\FieldWidget\DateTimeDefaultWidget.
 */

namespace Drupal\date_time_picker\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeWidgetBase;

/**
 * Plugin implementation of the 'date_time_picker' widget.
 *
 * @FieldWidget(
 *   id = "date_time_picker",
 *   label = @Translation("Date Time Picker"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DateTimePickerWidget extends DateTimeWidgetBase implements ContainerFactoryPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      $date_value = $values[$key]['value'];
      $date_value = str_replace(' ', 'T', $date_value);
      $values[$key]['value'] = $date_value;
    }
    return $values;
  }
  /**
   * The date format storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityStorageInterface $date_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->dateStorage = $date_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity.manager')->getStorage('date_format')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $date_vals = $items->getValue();
    foreach ($date_vals as $key => $date) {
      $date = str_replace('T', ' ', $date);
      $date_vals[$key] = $date;
    }
    $form['#attached']['drupalSettings']['default_dates'] = $date_vals;
    $element['value'] = array(
      '#type' => 'date_time_picker',
    );
    return $element;
  }

}
