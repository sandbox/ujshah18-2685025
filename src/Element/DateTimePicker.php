<?php

/**
 * @file
 * Contains \Drupal\date_time_picker\Element\DateTimePicker.
 */

namespace Drupal\date_time_picker\Element;

use \Drupal\Core\Render\Element\FormElement;
use \Drupal\Core\Render\Element;
use \Drupal\Core\Form\FormStateInterface;

/**
 * Provides a DateTimePicker form element.
 *
 * @FormElement("date_time_picker")
 */
class DateTimePicker extends FormElement {
  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return array(
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#maxlength' => 512,
      '#process' => array(
        array($class, 'processPicker'),
      ),
      '#pre_render' => array(
        array($class, 'preRenderPicker'),
      ),
      '#theme' => 'input__textfield',
      '#theme_wrappers' => array('form_element'),
    );

  }

  /**
   * Prepares a #type 'date_time_picker' render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderPicker($element) {
    Element::setAttributes($element, array('id', 'name', 'size'));
    static::setAttributes($element, array('form-text'));
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function processPicker(&$element, FormStateInterface $form_state, &$complete_form) {
    $complete_form['#attached']['drupalSettings']['date_time_picker'][$element['#id']] = json_encode($element['#settings']);
    $complete_form['#attached']['library'][] = 'date_time_picker/picker';
    return $element;
  }

  /**
   * Return default settings for date_time_picker. Pass in values to override defaults.
   *
   * @param array $values
   *   Some Desc.
   *
   * @return array
   *   Some Desc.
   */
  public static function settings(array $values = array()) {
    $settings = array(
      'lang' => 'en',
    );

    return array_merge($settings, $values);
  }

}
